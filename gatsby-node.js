/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

const _ = require(`lodash`)
const Promise = require(`bluebird`)
const path = require(`path`)
const slash = require(`slash`)

/**
 * Create pages from GraphQL queries.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/#createPages
 */
exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions

  // ==== PAGES ====
  return new Promise((resolve, reject) => {
    /**
     * Query data with GraphQL.
     *
     * See: https://www.gatsbyjs.org/docs/graphql/
     */
    graphql(`
      {
        allWordpressPage {
          edges {
            node {
              id
              link
            }
          }
        }
      }
    `)
      .then(result => {
        if (result.errors) {
          console.log(result.errors)
          reject(result.errors)
        }

        const template = path.resolve(`./src/templates/page.js`)

        _.each(result.data.allWordpressPage.edges, edge => {
          const node = edge.node
          const link = new URL(node.link) // Use link instead of slug to preserve WordPress url structure.

          /**
           * Create page.
           *
           * See: https://www.gatsbyjs.org/docs/actions/#createPage
           */
          createPage({
            path: link.pathname, // Pathname should have surrounding slashes already.
            component: slash(template),
            context: {
              id: node.id,
            },
          })
        })

        // Should end createPages.
        // resolve()
      })
      // ==== END PAGES ====

      // ==== POSTS ====
      .then(() => {
        graphql(`
          {
            allWordpressPost {
              edges {
                node {
                  id
                  link
                }
              }
            }
          }
        `).then(result => {
          if (result.errors) {
            console.log(result.errors)
            reject(result.errors)
          }

          const template = path.resolve(`./src/templates/post.js`)

          _.each(result.data.allWordpressPost.edges, edge => {
            const node = edge.node
            const link = new URL(node.link)

            createPage({
              path: link.pathname,
              component: slash(template),
              context: {
                id: node.id,
              },
            })
          })

          // Should end createPages.
          resolve()
        })
      })
    // ==== END POSTS ====
  })
}
