import React from 'react'
import { graphql } from 'gatsby'
import Layout from '../components/layout'
import preview from '../utils/preview'

export default ({ data }) => {
  const node = preview(data.wordpressPost)

  return (
    <Layout>
      <h1 dangerouslySetInnerHTML={{ __html: node.title }} />
    </Layout>
  )
}

export const query = graphql`
  query($id: String!) {
    wordpressPost(id: { eq: $id }) {
      title
    }
  }
`
