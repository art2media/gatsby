import { createGlobalStyle } from 'styled-components'

import {
  OpenSans300Woff2,
  OpenSans300Woff,
  OpenSans300ItalicWoff2,
  OpenSans300ItalicWoff,
  OpenSansRegularWoff2,
  OpenSansRegularWoff,
  OpenSansItalicWoff2,
  OpenSansItalicWoff,
  OpenSans600Woff2,
  OpenSans600Woff,
  OpenSans600ItalicWoff2,
  OpenSans600ItalicWoff,
  OpenSans700Woff2,
  OpenSans700Woff,
  OpenSans700ItalicWoff2,
  OpenSans700ItalicWoff,
  OpenSans800Woff2,
  OpenSans800Woff,
  OpenSans800ItalicWoff2,
  OpenSans800ItalicWoff,
} from '../utils/fonts'

const GlobalStyle = createGlobalStyle`
  @font-face {
    font-family: 'Open Sans';
    src: local('Open Sans Light'), local('OpenSans-Light'),
      url(${OpenSans300Woff2}) format('woff2'),
      url(${OpenSans300Woff}) format('woff');
    font-style: normal;
    font-weight: 300;
  }
  @font-face {
    font-family: 'Open Sans';
    src: local('Open Sans Light Italic'), local('OpenSans-LightItalic'),
      url(${OpenSans300ItalicWoff2}) format('woff2'),
      url(${OpenSans300ItalicWoff}) format('woff');
    font-style: italic;
    font-weight: 300;
  }
  @font-face {
    font-family: 'Open Sans';
    src: local('Open Sans Regular'), local('OpenSans-Regular'),
      url(${OpenSansRegularWoff2}) format('woff2'),
      url(${OpenSansRegularWoff}) format('woff');
    font-style: normal;
    font-weight: 400;
  }
  @font-face {
    font-family: 'Open Sans';
    src: local('Open Sans Italic'), local('OpenSans-Italic'),
      url(${OpenSansItalicWoff2}) format('woff2'),
      url(${OpenSansItalicWoff}) format('woff');
    font-style: italic;
    font-weight: 400;
  }
  @font-face {
    font-family: 'Open Sans';
    src: local('Open Sans SemiBold'), local('OpenSans-SemiBold'),
      url(${OpenSans600Woff2}) format('woff2'),
      url(${OpenSans600Woff}) format('woff');
    font-style: normal;
    font-weight: 600;
  }
  @font-face {
    font-family: 'Open Sans';
    src: local('Open Sans SemiBold Italic'), local('OpenSans-SemiBoldItalic'),
      url(${OpenSans600ItalicWoff2}) format('woff2'),
      url(${OpenSans600ItalicWoff}) format('woff');
    font-style: italic;
    font-weight: 600;
  }
  @font-face {
    font-family: 'Open Sans';
    src: local('Open Sans Bold'), local('OpenSans-Bold'),
      url(${OpenSans700Woff2}) format('woff2'),
      url(${OpenSans700Woff}) format('woff');
    font-style: normal;
    font-weight: 700;
  }
  @font-face {
    font-family: 'Open Sans';
    src: local('Open Sans Bold Italic'), local('OpenSans-BoldItalic'),
      url(${OpenSans700ItalicWoff2}) format('woff2'),
      url(${OpenSans700ItalicWoff}) format('woff');
    font-style: italic;
    font-weight: 700;
  }
  @font-face {
    font-family: 'Open Sans';
    src: local('Open Sans ExtraBold'), local('OpenSans-ExtraBold'),
      url(${OpenSans800Woff2}) format('woff2'),
      url(${OpenSans800Woff}) format('woff');
    font-style: normal;
    font-weight: 800;
  }
  @font-face {
    font-family: 'Open Sans';
    src: local('Open Sans ExtraBold Italic'), local('OpenSans-ExtraBoldItalic'),
      url(${OpenSans800ItalicWoff2}) format('woff2'),
      url(${OpenSans800ItalicWoff}) format('woff');
    font-style: italic;
    font-weight: 800;
  }
  @font-face {
    font-family: 'Open Sans';
    src: local('Open Sans ExtraBold'), local('OpenSans-ExtraBold'),
      url(${OpenSans800Woff2}) format('woff2'),
      url(${OpenSans800Woff}) format('woff');
    font-style: normal;
    font-weight: 800;
  }
  @font-face {
    font-family: 'Open Sans';
    src: local('Open Sans ExtraBold Italic'), local('OpenSans-ExtraBoldItalic'),
      url(${OpenSans800ItalicWoff2}) format('woff2'),
      url(${OpenSans800ItalicWoff}) format('woff');
    font-style: italic;
    font-weight: 800;
  }

  *,
  *::before,
  *::after {
    outline: none;
    box-sizing: border-box;
  }
`

export { GlobalStyle }
