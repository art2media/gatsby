import React from 'react'
import PropTypes from 'prop-types'
import { StaticQuery, graphql } from 'gatsby'

import i18n from '../utils/i18n'

import { ThemeProvider } from 'styled-components'
import { Normalize } from 'styled-normalize'
import { GlobalStyle } from './layout.styled'

import Header from './header'

const theme = {
  mainColor: `#663399`,
  maxWidth: `960`,
  maxPadding: `40`,
}

const Layout = ({ children }) => (
  <StaticQuery
    query={graphql`
      query {
        site {
          siteMetadata {
            siteUrl
          }
        }
        wordpressSiteMetadata {
          name
        }
        wordpressNetsbyMetadata {
          wpml {
            default
            active
            default_dir
          }
        }
      }
    `}
    render={data => {
      const site = data.site.siteMetadata
      const wpml = data.wordpressNetsbyMetadata.wpml

      return (
        <ThemeProvider theme={theme}>
          <>
            {i18n(wpml.default, wpml.active, wpml.default_dir, site.siteUrl)}

            <Normalize />
            <GlobalStyle />

            <Header siteTitle={data.wordpressSiteMetadata.name} />

            {children}
          </>
        </ThemeProvider>
      )
    }}
  />
)

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
