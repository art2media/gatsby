import i18n from 'i18next'
import LanguageDetector from 'i18next-browser-languagedetector'

export default (defaultLng, activeLng, defaultDir, siteUrl) => {
  if (activeLng.length > 1) {
    i18n.use(LanguageDetector).init(
      {
        /**
         * Configure i18next options.
         *
         * See: https://www.i18next.com/overview/configuration-options
         */

        debug: false,
        fallbackLng: defaultLng,

        interpolation: {
          escapeValue: false, // Not needed for React.
        },

        // initImmediate: false,
      },
      () => {
        // Directly called after language is detected and saved to localStorage.
        if (
          typeof window !== `undefined` &&
          window.localStorage &&
          window.location
        ) {
          const userLang = window.localStorage.getItem('i18nextLng').split('-')
          const path = window.location.pathname
          const pathLang = activeLng.filter(lang =>
            path.startsWith(`/${lang}/`)
          )

          // Redirect user to a path whose language might be understandable, but only:
          // - If default language is a directory AND path does not contain a translated path AND user speaks default site lang.
          // - OR if path does not contain a translated path AND user does not speak the default site lang.
          if (
            (defaultDir &&
              pathLang.length === 0 &&
              userLang[0] === defaultLng) ||
            (pathLang.length === 0 && userLang[0] !== defaultLng)
          ) {
            window.location.replace(`${siteUrl}/${userLang[0]}${path}`)
          }
        }
      }
    )
  }
}
