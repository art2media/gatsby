import axios from 'axios'

export default node => {
  if (
    typeof window !== `undefined` &&
    window.location.search &&
    process.env.NODE_ENV === `production`
  ) {
    const urlParams = new URLSearchParams(window.location.search)
    const previewId = urlParams.get(`preview`)
    const previewToken = urlParams.get(`token`)

    if (previewId && previewToken) {
      return axios({
        method: `get`,
        url: `${process.env.URL}/api/netsby/v1/preview`,
        headers: {
          'Content-Type': 'application/json',
        },
        data: {
          preview: previewId,
          token: previewToken,
          node: node,
        },
      })
    }
  }

  return node
}
