/**
 * Implement Gatsby's configuration options in this file.
 *
 * See: https://www.gatsbyjs.org/docs/gatsby-config/
 */

// Enable environment variables.
require(`dotenv`).config({
  path: `.env.${process.env.NODE_ENV}`,
})

const proxy = require(`http-proxy-middleware`)

module.exports = {
  siteMetadata: {
    author: `@cardiv`,
    siteUrl: process.env.URL, // Used to generate sitemaps.
  },
  developMiddleware: app => {
    app.use(
      `/api/`,
      proxy({
        target: `https://${process.env.WORDPRESS_BASEURL}`,
        changeOrigin: true,
        secure: false,
        pathRewrite: {
          '/api/': `/wp-json/`,
        },
      })
    )
  },
  plugins: [
    /**
     * Generate a default sitemap.
     *
     * See: https://www.gatsbyjs.org/packages/gatsby-plugin-sitemap/
     */
    `gatsby-plugin-sitemap`,

    /**
     * Add Lodash Webpack and Babel plugins.
     *
     * See: https://www.gatsbyjs.org/packages/gatsby-plugin-lodash/
     */
    `gatsby-plugin-lodash`,

    /**
     * Pull data from WordPress REST API.
     *
     * See: https://www.gatsbyjs.org/packages/gatsby-source-wordpress/
     */
    {
      resolve: `gatsby-source-wordpress`,
      options: {
        baseUrl: process.env.WORDPRESS_BASEURL,
        protocol: `https`,
        hostingWPCOM: false,
        useACF: true,
        verboseOutput: false, // Debug API endpoints if set to true.
        includedRoutes: [
          // Fetch all endpoints within these routes.
          `**/*/*/metadata`,
          `**/*/*/media`,
          `**/*/*/pages`,
          `**/*/*/posts`,
          `**/*/*/categories`,
          `**/*/*/tags`,
          `**/*/*/taxonomies`,
          `**/*/*/users`,
        ],
        excludedRoutes: [], // Fine tune fetched endpoints. Example: /*/*/posts/1456
        searchAndReplaceContentUrls: {
          sourceUrl: process.env.SOURCE_URL,
          replacementUrl: process.env.URL,
        },
      },
    },

    /**
     * Load Google Tag Manager on initial page/app load.
     *
     * See: https://www.gatsbyjs.org/packages/gatsby-plugin-google-tagmanager/
     */
    {
      resolve: `gatsby-plugin-google-tagmanager`,
      options: {
        id: process.env.GTM_ID,
        includeInDevelopment: false,
      },
    },

    /**
     * Support for Netlify _headers and _redirects files.
     *
     * See: https://www.gatsbyjs.org/packages/gatsby-plugin-netlify/
     */
    `gatsby-plugin-netlify`,

    /**
     * Add component styling.
     *
     * See: https://www.gatsbyjs.org/packages/gatsby-plugin-styled-components/
     */
    `gatsby-plugin-styled-components`,

    /**
     * Support for importing svg files as components.
     *
     * See: https://www.gatsbyjs.org/packages/gatsby-plugin-styled-components/
     */
    `gatsby-plugin-svgr`,

    /**
     * Support for Less stylesheets.
     *
     * See: https://www.gatsbyjs.org/packages/gatsby-plugin-less/
     */
    {
      resolve: `gatsby-plugin-less`,
      options: {
        javascriptEnabled: true, // Evaluate inline JavaScript.
      },
    },

    /**
     * Use Ant Design library.
     *
     * See: https://www.gatsbyjs.org/packages/gatsby-plugin-antd/
     */
    {
      resolve: `gatsby-plugin-antd`,
      options: {
        style: true, // Use Less styling.
      },
    },

    /**
     * Control document head with React Helmet.
     *
     * See: https://www.gatsbyjs.org/packages/gatsby-plugin-react-helmet/
     */
    `gatsby-plugin-react-helmet`,

    /**
     * Create File nodes for images.
     *
     * See: https://www.gatsbyjs.org/packages/gatsby-source-filesystem/
     */
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/assets/images`,
      },
    },

    /**
     * Enable image processing with the Sharp library.
     *
     * See: https://www.gatsbyjs.org/packages/gatsby-plugin-sharp/
     */
    `gatsby-plugin-sharp`,

    /**
     * Create ImageSharp nodes with the Sharp image processing library.
     *
     * See: https://www.gatsbyjs.org/packages/gatsby-transformer-sharp/
     */
    `gatsby-transformer-sharp`,

    /**
     * Enable 'add to home screen' feature.
     *
     * See: https://www.gatsbyjs.org/packages/gatsby-plugin-manifest/
     */
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `GatsbyJS`,
        short_name: `Gatsby`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`, // Control visible ui elements.
        icon: `./src/assets/images/gatsby-icon.png`,
      },
    },

    /**
     * Add service worker for offline support.
     *
     * See: https://www.gatsbyjs.org/packages/gatsby-plugin-offline/
     */
    `gatsby-plugin-offline`,
  ],
}
