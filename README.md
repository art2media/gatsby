<p align="center">
  <a href="https://www.gatsbyjs.org">
    <img alt="Gatsby" src="https://www.gatsbyjs.org/monogram.svg" width="60" />
  </a>
</p>
<h1 align="center">
  Gatsby Starter
</h1>

A starter configured for the Ant Design component library and WordPress as a headless CMS. It is best used with the [Netsby](https://gitlab.com/art2media/netsby) theme for WordPress.

Gatsby themes will eventually replace this starter.

## Features

- Ant Design component library
- source plugin for WordPress
- examples of createPages API with pre-built templates
- image processing library (Sharp)
- React Helmet document head component
- manifest and offline support
